# Docker OpenLDAP

This repository contains an OpenLDAP installation based on a CentOS base image.

[![pipeline status](https://gitlab.com/avisi/pec/openldap/badges/master/pipeline.svg)](https://gitlab.com/avisi/pec/openldap/commits/master)

---

### Tags

| Tag | Notes |
|-----|-------|
| `2.4.44` | OpenLDAP v2.4.44 |
| `latest` | Latest build of v2.4.44 |

## Usage

Example docker-compose file:
```yml
version: '3'
services:
  openldap:
    image: registry.gitlab.com/avisi/pec/openldap:2.4.44
    ports: ['389:389']
    environment:
      LDAP_ROOTPW: changeit
```

## Issues

All issues should be reported in the [Gitlab issue tracker](https://gitlab.com/avisi/pec/openldap/issues).
