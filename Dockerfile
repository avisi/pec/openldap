FROM registry.gitlab.com/avisi/base/centos:7

EXPOSE 389 636

ARG LDAP_VERSION=2.4.44-20.el7

ENV LDAP_SUFFIX="dc=avisi,dc=nl"
ENV LDAP_ROOTDN="cn=admin,${LDAP_SUFFIX}" \
    LDAP_ROOTPW="changeit"

ARG DATA_DIR="/data"
ENV CONFIG_DIR="${DATA_DIR}/config" \
    DATABASE_DIR="${DATA_DIR}/database" \
    LDIF_DIR="/ldif"

USER root
RUN yum install -y gettext openldap-servers-$LDAP_VERSION openldap-clients-$LDAP_VERSION && yum clean all \
    && tar cfz /etc/openldap/config.tgz -C /etc/openldap/slapd.d . && rm -rf /etc/openldap/slapd.d

VOLUME $DATA_DIR
VOLUME $LDIF_DIR

COPY bin/substitute /usr/local/bin/
COPY config/* /etc/openldap/config/
COPY entrypoint.sh /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
