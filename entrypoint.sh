#!/usr/bin/env bash

bootstrap_ldap() {
    mkdir -p $CONFIG_DIR $DATABASE_DIR
    tar zxf /etc/openldap/config.tgz -C $CONFIG_DIR
    cp /usr/share/openldap-servers/DB_CONFIG.example $DATABASE_DIR/DB_CONFIG
    chown -R ldap:ldap $DATABASE_DIR

    start_background_proces

    ldapadd -Q -Y EXTERNAL -H ldapi:/// -f /etc/openldap/schema/cosine.ldif
    ldapadd -Q -Y EXTERNAL -H ldapi:/// -f /etc/openldap/schema/nis.ldif
    ldapadd -Q -Y EXTERNAL -H ldapi:/// -f /etc/openldap/schema/inetorgperson.ldif
    ldapadd -Q -Y EXTERNAL -H ldapi:/// -f /etc/openldap/schema/ppolicy.ldif

    substitute "/etc/openldap/config/root.ldif" "LDAP_ROOTPW_ENC=$(slappasswd -h {SSHA} -s ${LDAP_ROOTPW})"
    ldapmodify -Q -Y EXTERNAL -H ldapi:/// -f "/etc/openldap/config/root.ldif"

    update_root_password
    modify_schemas

    stop_background_proces

    echo
    echo 'OpenLDAP init process complete; ready for start up.'
    echo
}

start_background_proces() {
    echo "Start OpenLDAP background proces"
    /usr/sbin/slapd -h "ldapi:///" -F $CONFIG_DIR -u ldap -g ldap
    echo "Waiting for OpenLDAP to start..."
    wait_seconds=5
    until test $((wait_seconds--)) -eq 0 -o -f "/run/openldap/slapd.pid" ; do sleep 1; done
    if [ ! -e /run/openldap/slapd.pid ]; then echo "start_background_proces: An error occurred"; exit 1; fi
}

stop_background_proces() {
    echo "Stop OpenLDAP background proces"
    kill -INT $(cat /run/openldap/slapd.pid)
}

update_root_password() {
    echo "Update LDAP root password"
    config_tmp_dir="/tmp/config"
    cp -rf "/etc/openldap/config" $config_tmp_dir
    substitute "${config_tmp_dir}/change_password.ldif" "LDAP_ROOTPW_ENC=$(slappasswd -h {SSHA} -s ${LDAP_ROOTPW})"
    ldapmodify -Q -Y EXTERNAL -H ldapi:/// -f "${config_tmp_dir}/change_password.ldif"
    rm -rf $config_tmp_dir
}

modify_schemas() {
    echo "Load custom ldif's"
    for f in $LDIF_DIR/*; do
        case "$f" in
            *.ldif)   echo "modify_schemas: running $f"; ldapmodify -Q -Y EXTERNAL -H ldapi:/// -f "$f";;
            *)        echo "modify_schemas: ignoring $f" ;;
        esac
        echo
    done
}

if [ ! -s "${DATABASE_DIR}/DB_CONFIG" ]; then
    echo "No LDAP (database) detected, starting bootstrap"
    echo "Bootstrapping LDAP..."
    bootstrap_ldap
  else
    echo "Detected existing LDAP instance, updating..."
    start_background_proces
    update_root_password
    modify_schemas
    stop_background_proces
fi

echo "Starting OpenLDAP..."
exec /usr/sbin/slapd -h "ldaps:/// ldap:///" -F $CONFIG_DIR -u ldap -g ldap -d 32768
